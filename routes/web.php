<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TodoController@index');
Route::get('/todos', 'TodoController@all');
Route::post('/todo', 'TodoController@create');
Route::get('/todo/{todo}', 'TodoController@toggle');
Route::post('/todo/{todo}', 'TodoController@update');
Route::get('/todo/edit/{todo}', 'TodoController@get');

Route::delete('/todo/{todo}', 'TodoController@remove');


Auth::routes();
