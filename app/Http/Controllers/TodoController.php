<?php

namespace App\Http\Controllers;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function index()
    {
        return view('welcome');
    }

    public function all()
    {
        return Todo::orderBy('completed_at', 'asc')->get();
    }

    public function get( Todo $todo )
    {
        return $todo;
    }

    public function create(Request $request)
    {
        $todo = new Todo();
        $todo->name = $request->get('name');
        $todo->save();

        return $todo;
    }

    public function update(Request $request, Todo $todo)
    {
        $todo->name = $request->get('name');
        $todo->save();

        return $todo;
    }

    public function toggle( Todo $todo ){
        if( $todo->completed_at !== null ) {
            $todo->completed_at = null;
        } else {
            $todo->completed_at = Carbon::now();
        }
        $todo->save();
        return $todo;
    }


    public function remove(Todo $todo)
    {
        $todo->delete();
    }

}
