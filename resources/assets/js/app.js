
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: require('./components/Todos'),
        name: 'home'
    },
    {
        path: '/todo/create',
        component: require('./components/CreateTodo'),
        name: 'create'
    },
    {
        path: '/todo/edit/:todo_id',
        component: require('./components/EditTodo'),
        name: 'edit'
    }
]

const router = new VueRouter({
    routes
})


const app = new Vue({
    router
}).$mount('#app')
